# Defined in - @ line 1
function ytdl 
  docker run --rm -it -v $PWD:/download jtdressel/youtube-dl:latest --add-metadata  --ignore-errors  --embed-thumbnail -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]' --newline --restrict-filenames --output '%(uploader)s-%(title)s-%(id)s.%(ext)s' --exec 'mv {} /download/' $argv;
end
