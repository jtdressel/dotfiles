function tmpush
	set tempParentDir (mktemp -d)
	if count $argv > /dev/null
		set tempDir $tempParentDir/$argv[1]
	else
		set tempDir $tempParentDir
	end
	mkdir -p $tempDir
	pushd "$tempDir"
end
