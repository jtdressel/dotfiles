# James dotfiles README.md

## To Apply

```
stow --dotfiles --restow --target="$HOME" general
```

or run `do-stow.sh`

## "From scratch"

TODO handle stow not being installed

```
mkdir "$HOME/dotfiles" && pushd "$HOME/dotfiles" && git clone https://gitlab.com/jtdressel/dotfiles.git . && stow --dotfiles --restow --target="$HOME" general && git remote set-url origin git@gitlab.com:jtdressel/dotfiles.git
```

## OSX Notes: 

### When the kext prompt is missing

Reboot into recovery

`sudo nvram "recovery-boot-mode=unused"; sudo reboot`

In recovery:


`spctl kext-consent disable`
`spctl kext-consent enable`

